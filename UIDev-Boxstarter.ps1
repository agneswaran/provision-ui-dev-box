Disable-UAC

$UseWSL = [Environment]::GetEnvironmentVariable("Boxstarter_UseWSL", "User")
$Distro = [Environment]::GetEnvironmentVariable("Boxstarter_Distro", "User")
$AutoProvisionWSL = [Environment]::GetEnvironmentVariable("Boxstarter_AutoProvisionWSL", "User")

$UnderlineEsc = [char]27

@"

$UnderlineEsc[4mParameters received from environment variables$UnderlineEsc[0m
- UseWSL => $UseWSL
- Distro => $Distro
- AutoProvisionWSL => $AutoProvisionWSL
"@

# Get the base URI path from the ScriptToCall value
$BaseUri = $Boxstarter['ScriptToCall']

$bstrappackage = "-bootstrapPackage"
$strpos = $BaseUri.IndexOf($bstrappackage)
$pathSeparator = "/"

$BaseUri = $BaseUri.Substring($strpos + $bstrappackage.Length)
$BaseUri = $BaseUri.TrimStart("'", " ")
$BaseUri = $BaseUri.TrimEnd("'", " ")
$BaseUri = $BaseUri.Substring(0, $BaseUri.LastIndexOf($pathSeparator))

Function Join-Parts
{
    Param 
    (
        $Parts = $null,
        $Separator = ''
    )

    ($Parts | Where-Object { $_ } | ForEach-Object { ([string]$_).trim($Separator) } | Where-Object { $_ } ) -join $Separator 
}

Function ExecuteScript {
    Param 
    (
            [string] $Script,
            [string] $Cmdlet,
            [string] $Arguments
    )

    $ScriptPath = Join-Parts -Separator '/' -Parts $BaseUri, $Script
    $Command = "$Cmdlet $Arguments"

    # Execute the result of the web request to register the cmdlet
    # Execute the registered cmdlet
    "Executing $ScriptPath"
    . { Invoke-WebRequest -useb $ScriptPath } | Invoke-Expression; Invoke-Expression $Command
}

If (Test-PendingReboot) {
    "Rebooting before commencing installation..."
    Start-Sleep -Seconds 3
    Invoke-Reboot
}

# Execute base install for Windows
"`nAttempting to install the standard set of Windows base tools"
ExecuteScript "Windows/Install-StandardTools.ps1"

# Execute VS Code install for Windows
"`nAttempting to install Visual Studio Code w/ recommended extensions"
ExecuteScript -Script "Windows/Install-VSCode.ps1"

If ($UseWSL -eq $true) {
    # Argument list
    $ArgumentList = "-Distro $Distro "

    If ($AutoProvisionWSL -eq $true) {
        $ArgumentList += "-ProvisionEnv "
    }

    # Execute base installation script for WSL
    "`nAttempting to enable the Windows Subsystem for Linux & install $Distro"
    ExecuteScript "WSL/Install_WSL_Distro.ps1" "Install-WSL" "$ArgumentList"
} Else {
    # Execute the fallback provisioner for Windows
    "`nAttempting to install rest of the requisite Windows tools"
}

@"

Clearing provisioning parameters
- UseWSL
- Distro
- AutoProvisionWSL

"@
[Environment]::SetEnvironmentVariable("Boxstarter_UseWSL", $null, "User")
[Environment]::SetEnvironmentVariable("Boxstarter_Distro", $null, "User")
[Environment]::SetEnvironmentVariable("Boxstarter_AutoProvisionWSL", $null, "User")

Enable-UAC
Enable-MicrosoftUpdate

RefreshEnv

"`nEnvironment provisioning complete`n"
