# . { Invoke-WebRequest -useb https://gitlab.com/agneswaran/provision-ui-dev-box/raw/release/Provision-UIDevBox.ps1 } | Invoke-Expression; Provision-UIDevBox -UseWSL -Distro Ubuntu -AutoProvisionWSL
# TODO: Decide if the function name needs to be refactored to approved verbs usage
Function Provision-UIDevBox {
[CmdletBinding()]
    <#
        .SYNOPSIS
        This script attempts to auto-provision a WDP UI development environment
        with a pre-installed software stack of recommended component & libraries

        .DESCRIPTION

        .PARAMETER UseWSL
        If the switch is provided, try to enable the Windows Subsystem for Linux &
        install a distro (see below)

        .PARAMETER Distro
        If the UseWSL switch is provided, the distro to be installed is mandatory
        The provisioning script for now supports only
        1. Debian
        2. Ubuntu 18.04

        .PARAMETER AutoProvisionWSL
        If the switch is provided, setup ansible on the Linux environment &
        try to run the provisioning playbook

        .EXAMPLE
        PS > . { Invoke-WebRequest -useb https://gitlab.com/agneswaran/provision-ui-dev-box/raw/release/Provision-UIDevBox.ps1 } | Invoke-Expression; Provision-UIDevBox
        This would be an example usage on Windows 7 (SOE 7)

        .EXAMPLE
        PS> . { Invoke-WebRequest -useb https://gitlab.com/agneswaran/provision-ui-dev-box/raw/release/Provision-UIDevBox.ps1 } | Invoke-Expression; Provision-UIDevBox -UseWSL -Distro Ubuntu
        This would be an example usage on Windows 10 (EOE) with WSL & no auto-provisioning of the distro

        .EXAMPLE
        PS> . { Invoke-WebRequest -useb https://gitlab.com/agneswaran/provision-ui-dev-box/raw/release/Provision-UIDevBox.ps1 } | Invoke-Expression; Provision-UIDevBox -UseWSL -Distro Ubuntu -AutoProvisionWSL
        This would be an example usage on Windows 10 (EOE) with WSL & auto-provisioning of the distro

        .NOTES
    #>
    Param
    (
	    [Parameter(Position = 0, HelpMessage = "Use Windows Subsystem for Linux (Supported only on Windows 10)")]
	    [switch] $UseWSL
    )

    DynamicParam {
        # Validate the usage of UseWSL against the actual OS version to establish support
        $WinEdition = (Get-WmiObject Win32_OperatingSystem).Caption
        $WinBuild = (Get-WmiObject Win32_OperatingSystem).BuildNumber

        Try {
            $WinVersion = (Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion' -Name ReleaseID -ErrorAction Stop).ReleaseID
        }
        Catch {
            $WinVersion = "N/A"
        }

        # If the current OS is not a Windows 10 variant do not proceed
        # TODO: Since WSL is available only for certain build nos & above
        #       detect before proceeding
        If ($UseWSL -and $WinEdition -notlike "*Windows 10*") {
            Throw "Your version of Windows does not support Windows Subsystem for Linux (WSL)"
        }

        # If OS is a Windows 10 variant then proceed
        If ($UseWSL) {
            # Expose our dynamic parameters when the condition is satisfied
            $ParamDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary

            # Create a new ParameterAttribute Object for Distro
            $DistroAttribute = New-Object System.Management.Automation.ParameterAttribute -Property @{
                Position = 1
                Mandatory = $true
                HelpMessage = "Choose a Linux distribution to install (Ubuntu or Debian)"
            }

            # Create an attributecollection object for the attribute just created
            $AttributeCollection = New-Object System.Collections.ObjectModel.Collection[System.Attribute]
 
            # Add the DistroAttribute to the attribute collection
            $AttributeCollection.Add($DistroAttribute)

            # Create a ValidateSetAttribute to restrain input to the configured Distros
            $ValidateSetAttribute = New-Object System.Management.Automation.ValidateSetAttribute("Ubuntu", "Debian")
            $AttributeCollection.Add($ValidateSetAttribute)
 
            # Add DistroParam specifying the attribute collection
            $DistroParam = New-Object System.Management.Automation.RuntimeDefinedParameter('Distro', [string], $AttributeCollection)
            $ParamDictionary.Add('Distro', $DistroParam)

            # Create a new ParameterAttribute Object for AutoProvisionWSL
            $AutoProvisionWSLAttribute = New-Object System.Management.Automation.ParameterAttribute -Property @{
                Position = 2
                Mandatory = $false
                HelpMessage = "Auto-provision the distro with a recommended software stack"
            }

            # Create a new attributecollection object for the attribute just created
            $AttributeCollection = New-Object System.Collections.ObjectModel.Collection[System.Attribute]
 
            # Add the AutoProvisionWSLAttribute to the attribute collection
            $AttributeCollection.Add($AutoProvisionWSLAttribute)

            # Add AutoProvisionWSLParam specifying the attribute collection
            $AutoProvisionWSLParam = New-Object System.Management.Automation.RuntimeDefinedParameter('AutoProvisionWSL', [switch], $AttributeCollection)
            $ParamDictionary.Add('AutoProvisionWSL', $AutoProvisionWSLParam)

            return $ParamDictionary
        }
    }

    Begin {
        # Write some fancy output to the console in tabular format
        $Selections = "" | Select-Object "Edition", "Version", "Build", "UseWSL", "Distro", "AutoProvisionWSL"

        $Selections.Edition = $WinEdition
        $Selections.Version = $WinVersion
        $Selections.Build = $WinBuild
        $Selections.UseWSL = $UseWSL
        $Selections.Distro = $PSBoundParameters.Distro
        $Selections.AutoProvisionWSL = $PSBoundParameters.AutoProvisionWSL -or $false

        $InstallInfo = @()

        $InstallInfo += $Selections

        $InstallInfo | Format-Table
    }

    Process {
        # Boxstarter default installation path
        $BoxstarterPath = 'C:\ProgramData\Boxstarter\BoxstarterShell.ps1'

        If (!(Test-Path $BoxstarterPath)) {
            <#
                Install Boxstarter for One-click installation
                Chocolatey will be installed if not found
            #>
            Write-Host "Installing Boxstarter...`n"
            . { Invoke-WebRequest -useb https://boxstarter.org/bootstrapper.ps1 } | Invoke-Expression; Get-Boxstarter -Force

            Write-Host "`nWaiting for environment refresh before continuing...`n"
            Start-Sleep -Seconds 3
        }

        # Get the current working directory
        $CWD = "$(Get-Location)"

        # Run the Boxstarter shell as an Administrator
        . $BoxstarterPath

        # Navigate to the working directory
        Set-Location "$CWD"

        # Have to pass parameters via environment variables
        # https://stackoverflow.com/questions/37576493/passing-parameters-to-boxstarter-via-install-boxstarterpackage
        "`nSetting provisioning parameters in temporary environment variables"
        "- UseWSL"
        "- Distro"
        "- AutoProvisionWSL`n"
        [Environment]::SetEnvironmentVariable("Boxstarter_UseWSL", $UseWSL, "User")
        [Environment]::SetEnvironmentVariable("Boxstarter_Distro", $PSBoundParameters.Distro, "User")
        [Environment]::SetEnvironmentVariable("Boxstarter_AutoProvisionWSL", ($PSBoundParameters.AutoProvisionWSL -or $false), "User")

        # Since we're running all commands henceforth within the Boxstarter shell
        # the native cmdlet, commands & helper are available in context
        RefreshEnv

        # Refactored the Start-Process usage to direct command invocation
        # since we're in the Boxstarter shell
        Write-Host "`nExecuting the boostrap package`n"
        Install-BoxstarterPackage -PackageName https://gitlab.com/agneswaran/provision-ui-dev-box/raw/release/UIDev-Boxstarter.ps1
    }
}
