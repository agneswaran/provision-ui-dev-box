# . { Invoke-WebRequest -useb https://gitlab.com/agneswaran/provision-ui-dev-box/raw/release/WSL/Install-WSL.ps1 } | Invoke-Expression; Install-WSL -Distro Debian -ProvisionEnv
Function Install-WSL {
[CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [string] $Distro,
    
        [Parameter(Mandatory = $false, ValueFromPipeline = $true)]
        [switch] $ProvisionEnv
    )

    Begin {
        $UnderlineEsc = [char]27
        
        "`n$UnderlineEsc[4mParameters received$UnderlineEsc[0m"
        "- Distro => $Distro"
        "- ProvisionEnv => $ProvisionEnv"

        # Define an enum for the supported distros
        Enum Distro {
            Ubuntu
            Debian
        }
        
        # Cast the input to its defined enum distro
        # Default to Ubuntu if input is invalid
        Try {
            [Distro]$SelectedDistro = [Distro]::$Distro
        }
        Catch {
            "`nInvalid distro chosen"
            "Defaulting to Ubuntu-1804 (Bionic-Beaver)"
        
            [Distro]$SelectedDistro = [Distro]::Ubuntu
        }
        
        # Initialise scope variables for the supported distros
        Switch ($SelectedDistro) {
            Ubuntu {
                $DistroName = "Ubuntu-1804 (Bionic-Beaver)"
                $AppX = "Ubuntu-1804"
                $StoreUri = "https://aka.ms/wsl-ubuntu-1804"
                $Executable = "Ubuntu1804"
                $PkgMan = "apt-get --yes"
                $PkgUpdateCmd = "update"
                $PkgUpgradeCmd = "upgrade"
        
                Break
            }
            Debian {
                $DistroName = "Debian-9 (stretch)"
                $AppX = "Debian-9"
                $StoreUri = "https://aka.ms/wsl-debian-gnulinux"
                $Executable = "Debian"
                $PkgMan = "apt-get --yes"
                $PkgUpdateCmd = "update"
                $PkgUpgradeCmd = "upgrade"
        
                Break
            }
            # OpenSUSE {
            #     $DistroName = "OpenSUSE-42 (Leap)"
            #     $AppX = "OpenSUSE-42"
            #     $StoreUri = "https://aka.ms/wsl-opensuse-42"
            #     $Executable = "openSUSE-42"
            #     $PkgMan = "zypper --non-interactive"
            #     $PkgUpdateCmd = "refresh"
            #     $PkgUpgradeCmd = "update"

            #     Break
            # }
        }
    }

    Process {
        # Get the base URI path from the ScriptToCall value
        $BaseUri = $Boxstarter['ScriptToCall']
        
        $bstrappackage = "-bootstrapPackage"
        $strpos = $BaseUri.IndexOf($bstrappackage)
        $pathSeparator = "/"
        
        $BaseUri = $BaseUri.Substring($strpos + $bstrappackage.Length)
        $BaseUri = $BaseUri.TrimStart("'", " ")
        $BaseUri = $BaseUri.TrimEnd("'", " ")
        $BaseUri = $BaseUri.Substring(0, $BaseUri.LastIndexOf($pathSeparator))
        
        Function Join-Parts
        {
            Param 
            (
                $Parts = $null,
                $Separator = ''
            )
        
            ($Parts | Where-Object { $_ } | ForEach-Object { ([string]$_).trim($Separator) } | Where-Object { $_ } ) -join $Separator 
        }
        
        Function ExecuteScript {
            Param 
            (
                    [string] $Script,
                    [string] $Cmdlet,
                    [string] $Arguments
            )

            $ScriptPath = Join-Parts -Separator '/' -Parts $BaseUri, $Script
            $Command = "$Cmdlet $Arguments"

            # Execute the result of the web request to register the cmdlet
            # Execute the registered cmdlet
            "Executing $ScriptPath"
            . { Invoke-WebRequest -useb $ScriptPath } | Invoke-Expression; Invoke-Expression $Command
        }
        
        Try {
            # Enable the Windows Subsystem for Linux
            "`nEnabling the Windows subsystem for Linux"
            Set-ItemProperty -Path HKLM:\Software\Microsoft\Windows\CurrentVersion\AppModelUnlock -Name AllowDevelopmentWithoutDevLicense -Value 1
            choco install Microsoft-Windows-Subsystem-Linux -source windowsfeatures
        
            # Reboot machine to refresh environment variables & Windows features
            If (Test-PendingReboot) {
                "Rebooting before commencing $DistroName installation..."
                Start-Sleep -Seconds 3
                Invoke-Reboot
            }
        
            <#
                Always attempt to start from drive root (C:)
            #>
            "`nNavigating to drive root"
            Set-Location /
        
            $PWD = "$(Get-Location)"
        
            # Abort WSL provisioning if running on H:\
            If ($PWD -notlike "C:\") {
                "`nAborting..."
                "Try executing the script again as an Administrator"
        
                Return
            }
        
            <# 
                WSL environment provisioning
                
                Figure out how to set this as HOME
                Depending on the OS build version 
                approach changes
        
                /etc/fstab
                /etc/wsl.conf
                /etc/passwd
        
                ----------------------------
                Anticipated contents
                ****************************
                1 Distros (Installed [distros.appx])
                2 .ssh
                3 .dotfiles
            #>
            $WSLExists = Test-Path "C:\WSL"
        
            If (-Not $WSLExists) {
                "`nCreating directory WSL"
                mkdir WSL > $null
            }
        
            "Navigating to C:\WSL"
            Set-Location ./WSL
        
            <# 
                C:\WSL\Distros ended up being just a download
                directory with the [distros.appx] files
        
                Intial approach of extracting the Appx to 
                a $DistroName directory is invalidated
                due to usage of Add-AppxPackage
            #>
            $WSLDistosExists = Test-Path "C:\WSL\Distros"
        
            If (-Not $WSLDistosExists) {
                "`nCreating directory Distros"
                mkdir Distros > $null
            }
        
            "Navigating to C:\WSL\Distros"
            Set-Location ./Distros
            
            <#
                1 Download the chosen distro
                2 Add-AppxPackage
                    a Cmdlet extracts a given package to C:\Users\<USERNAME>\AppData\Local\Packages
                    b Registers the distro within Windows
                    c Adds the executable to the user PATH
                    d Adds a Start Menu short-cut
            #>
            "`nDownloading $DistroName from $StoreUri"
            (New-Object System.Net.WebClient).DownloadFile($StoreUri, "C:\WSL\Distros\$AppX.appx")
        
            "Registering $AppX with Windows"
            Add-AppxPackage -Path "C:\WSL\Distros\$AppX.appx"
        
            Set-Location ..
        
            "`nInstalling $DistroName"
            Start-Process "$Executable" -ArgumentList "install --root" -Wait
        
            # Call user provisioning script
            # Provision_User.ps1
            # This would be a mandatory step
            "`nProvisioning a user for WSL"
            ExecuteScript "WSL/Provision_User.ps1" "Provision-User" "-WSLExecutable $Executable"
        
            # Make this step optional
            # Some users may want to experiment
            # with a customised a setup
            # For everyone else use the standard
            # provisioning template & playbook
            If ($ProvisionEnv -eq $true) {
                # Call distro provisioning script
                # Provision_Distro.ps1
                "`nProvisioning default recommeded packages"
                ExecuteScript "WSL/Provision_Distro.ps1" "Provision-Distro" "-WSLExecutable $Executable -PkgMan `"$PkgMan`" -PkgUpdateCmd $PkgUpdateCmd -PkgUpgradeCmd $PkgUpgradeCmd"
            }
        }
        Catch {
            "`nException occurred while setting up your WSL environemnt`n"
            $_.Exception.GetType().FullName, $_.Exception.Message
        }
    }
}
