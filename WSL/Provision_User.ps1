Function Provision-User {
[CmdletBinding()]

    Param
    (
    	[Parameter(Mandatory = $true, Position = 0)]
    	[string] $WSLExecutable
    )

    Begin {
        $UnderlineEsc = [char]27
        
        "`n$UnderlineEsc[4mParameters received$UnderlineEsc[0m"
        "- WSLExecutable => $WSLExecutable"
        
        $User = $env:USERNAME
        $User_Passwd = $User+":P@ssw0rd"
    }

    Process {
        "`nAdding user $UnderlineEsc[4m$User$UnderlineEsc[0m to the shell"
        Start-Process "$WSLExecutable" -ArgumentList "run adduser $User --disabled-password --gecos ''" -Wait
        
        "`nSetting a default password $UnderlineEsc[4mP@ssw0rd$UnderlineEsc[0m for $User"
        "Use $UnderlineEsc[4mpasswd$UnderlineEsc[0m to change the default password when logging on for the first time"
        Start-Process "$WSLExecutable" -ArgumentList "run echo '$User_Passwd' | sudo chpasswd" -Wait
        
        "`nAdding $User to the $UnderlineEsc[4msudo$UnderlineEsc[0m group"
        Start-Process "$WSLExecutable" -ArgumentList "run usermod -aG sudo $User" -Wait
        
        "Set $User as the default user for the shell"
        Start-Process "$WSLExecutable" -ArgumentList "config --default-user $User" -Wait
    }
}
