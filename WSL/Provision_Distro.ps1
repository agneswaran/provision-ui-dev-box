Function Provision-Distro {
[CmdletBinding()]

    Param
    (
    	[Parameter(Mandatory = $true, Position = 0)]
    	[string] $WSLExecutable,
    
    	[Parameter(Mandatory = $true, Position = 1)]
    	[string] $PkgMan,
    
    	[Parameter(Mandatory = $false, Position = 2)]
    	[string] $PkgUpdateCmd,
    
    	[Parameter(Mandatory = $false, Position = 3)]
    	[string] $PkgUpgradeCmd
    )

    Begin {
        $UnderlineEsc = [char]27
        
        "`n$UnderlineEsc[4mParameters received$UnderlineEsc[0m"
        "- WSLExecutable => $WSLExecutable"
        "- PkgMan => $PkgMan"
        "- PkgUpdateCmd => $PkgUpdateCmd"
        "- PkgUpgradeCmd => $PkgUpgradeCmd"
    }

    Process {
        "`nUpdate the system's available package database"
        Start-Process "$WSLExecutable" -ArgumentList "run echo 'P@ssw0rd' | sudo -S $PkgMan $PkgUpdateCmd" -Wait
        
        "Upgrade all installed packages to their latest versions"
        Start-Process "$WSLExecutable" -ArgumentList "run echo 'P@ssw0rd' | sudo -S $PkgMan $PkgUpgradeCmd" -Wait
        
        "`nInstall the essential package dependencies"
        Start-Process "$WSLExecutable" -ArgumentList "run echo 'P@ssw0rd' | sudo -S $PkgMan install python-pip" -Wait
        
        "Installing $UnderlineEsc[4mansible$UnderlineEsc[0m to provision the WSL environment via a playbook"
        Start-Process "$WSLExecutable" -ArgumentList "run echo 'P@ssw0rd' | sudo -S pip install ansible" -Wait
    }
}
