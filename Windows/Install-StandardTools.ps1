# Network utilities
@"

Installing network utilities
    wget
    curl

"@
choco install -r -y wget
choco install -r -y curl

# Archiving utilities
@"

Installing archiving utilities
    7zip

"@
choco install -r -y 7zip

# Postman - API Development Environment
@"

Installing API development/testing tools
    postman

"@
choco install -r -y postman

# Vivaldi - Browser 
# Required for React & Redux Devtools extension
@"

Installing browsers
    Vivaldi

"@
choco install -r -y vivaldi

# TODO:     Move GIT installation to its own file
#           & add default git config options

# FIXME:    Machine reboots everytime GIT
#           installation starts but not during
#           a UI manual installation

# Git
@"

Installing the version control system
    git

"@
choco install -r -y git

# Refresh environment variables
RefreshEnv
