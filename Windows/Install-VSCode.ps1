# Visual Studio Code
@"

Installing Visual Studio Code

"@
choco install -r -y vscode

# Refreshing environment variables for "code" command to be found
RefreshEnv

# Visual Studio Code Extensions
@"

Installing Visual Studio Code extensions 
    Editor config
    Commitizen
    Prettier
    ESLint
    GitLens
    TODO Tree
    TODO Highlight
    Code spell checker
    Material icon theme
    Bracket pair colorizer

"@
code --install-extension CoenraadS.bracket-pair-colorizer
code --install-extension streetsidesoftware.code-spell-checker
code --install-extension EditorConfig.EditorConfig
code --install-extension dbaeumer.vscode-eslint
code --install-extension eamodio.gitlens
code --install-extension PKief.material-icon-theme
code --install-extension esbenp.prettier-vscode
code --install-extension Gruntfuggly.todo-tree
code --install-extension wayou.vscode-todo-highlight
code --install-extension KnisterPeter.vscode-commitizen
